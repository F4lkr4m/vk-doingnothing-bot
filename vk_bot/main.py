import click
from classes.bot import Bot
from classes.database import Database


@click.command()
@click.option('--start', is_flag=True)
def main(start):
    if start:
        bot = Bot()
        bot.start()


if __name__ == '__main__':
    main()

import sqlite3
import os


filepath = 'db.db'


def check_db_file_exists(filepath1):
    """Check existing of db on this filepath, if not - create file"""
    check_file_exist = os.path.exists(filepath1)
    if check_file_exist:
        f = open(filepath1, 'w')
        f.close()


class Database:
    def __init__(self):
        check_db_file_exists(filepath)
        self.db = sqlite3.connect(filepath)
        self.cur = self.db.cursor()
        self.check_db_exists()
        self.script()

    def insert_record_in_users(self, user_id, name, surname, rules):
        self.cur.execute('SELECT * FROM users WHERE (user_id IS ? AND name IS ? AND surname IS ?)',
                         (user_id, name, surname))
        row = self.cur.fetchone()
        if row is None:
            self.cur.execute('INSERT INTO users(user_id, name, surname, rules) VALUES (?,?,?,?)',
                             (user_id, name, surname, rules))
            self.db.commit()

        self.cur.execute('SELECT * FROM user_states WHERE user_id=%d' % user_id)
        row = self.cur.fetchone()
        if row is None:
            self.cur.execute('INSERT INTO user_states(user_id, user_state) VALUES (?,?)', (user_id, 0))
            self.db.commit()

    def insert_file_in_task_files(self, name, subject, filename):
        self.cur.execute('SELECT * FROM task_files WHERE (name IS ? AND subject IS ? AND file IS ?)',
                         (name, subject, filename))
        row = self.cur.fetchone()
        if row is None:
            self.cur.execute('INSERT INTO task_files(name, subject, file) VALUES (?,?,?)', (name, subject, filename))
            self.db.commit()

    def change_user_state(self, user_id, state_to_insert):
        self.cur.execute('UPDATE user_states SET user_state=%d WHERE user_id=%d' % (state_to_insert, user_id))
        self.db.commit()

    def get_user_state(self, user_id):
        self.cur.execute('SELECT user_state FROM user_states WHERE user_id=%d' % user_id)
        return self.cur.fetchone()[0]

    def get_filepath(self, name, subject):
        self.cur.execute('SELECT file FROM task_files WHERE (name IS ? AND subject IS ?)', (name, subject))
        row = self.cur.fetchone()
        if row is None:
            return 'error'
        else:
            return row[0]

    def delete_record_in_users(self, user_id):
        cmd = 'SELECT * FROM users WHERE user_id =%d' % user_id
        self.cur.execute(cmd)
        row = self.cur.fetchone()
        if row is not None:
            cmd = 'DELETE FROM users WHERE user_id= %d' % user_id
            self.cur.execute(cmd)
            self.db.commit()

    def convertToBinaryData(self, filename):
        # Convert digital data to binary format
        with open(filename, 'rb') as file:
            blobData = file.read()
        return blobData

    def insertBLOB(self, name, task):
        sqlite_insert_blob_query = """ INSERT INTO task_files(name, file) VALUES (?, ?)"""

        task = self.convertToBinaryData(task)
        # Convert data into tuple format
        data_tuple = (name, task)
        self.cur.execute(sqlite_insert_blob_query, data_tuple)
        self.db.commit()
        print("Image and file inserted successfully as a BLOB into a table")

    def writeTofile(self, data, filename):
        # Convert binary data to proper format and write it on Hard Disk
        with open(filename, 'wb') as file:
            file.write(data)
            print("Stored blob data into: ", filename, "\n")

    def readBlobData(self, filename):
        sql_fetch_blob_query = """SELECT * from task_files where name = ?"""
        self.cur.execute(sql_fetch_blob_query, (filename,))
        record = self.cur.fetchall()
        for row in record:
            print("Name = ", row[0])
            name = row[0]
            file = row[1]

            print("Storing employee image and resume on disk \n")
            file_path = name + ".pdf"
            self.writeTofile(file, file_path)

    def script(self):
        subjects = ['phys', 'paovs', 'termeh', 'math']

        dir_with_data = 'data_files'

        for subject in subjects:
            for roots, dirs, files in os.walk(dir_with_data):
                for file in files:
                    if file.split('_')[0] == subject:
                        name = file.split('_')[1].split('.')[0]
                        subj = file.split('_')[0]
                        filename = 'data_files/' + file
                        print('name:' + file.split('_')[1].split('.')[0])
                        print('subj:' + file.split('_')[0])
                        print('filename:' + filename)
                        self.insert_file_in_task_files(name, subject, filename)

    def _init_db(self):
        """Initialize database"""
        with open('classes/createdb.sql', 'r') as f:
            sql = f.read()
        self.cur.executescript(sql)
        self.db.commit()
        self.script()

    def check_db_exists(self):
        """Check, was database init or not, if not - initialize"""
        self.cur.execute("SELECT name FROM sqlite_master "
                         "WHERE type='table' AND name='users'")
        table_exists = self.cur.fetchall()
        if table_exists:
            return
        self.cur.execute("SELECT name FROM sqlite_master "
                         "WHERE type='table' AND name='user_states'")
        table_exists = self.cur.fetchall()
        if table_exists:
            return
        self.cur.execute("SELECT name FROM sqlite_master "
                         "WHERE type='table' AND name='task_files'")
        table_exists = self.cur.fetchall()
        if table_exists:
            return
        self._init_db()

import vk_api
import random
import requests
import json
from classes.database import Database

INIT_STATE = 0
HW_CHOOSING_STATE = 1
TERMEH_STATE = 2
PHYS_LAB_STATE = 3
PHYS_HW_STATE = 4
PAOVS_STATE = 5
MATH_STATE = 6

INIT_KEYBOARD_MENU = 'jsons/init_keyboard.json'
HW_CHOOSING_SUBJECT_MENU = 'jsons/hw_choosing_menu.json'
TERMEH_MENU = 'jsons/termeh_menu.json'
PHYS_LAB_MENU = 'jsons/phys_lab_menu.json'
PHYS_HW_MENU = 'jsons/phys_hw_menu.json'
PAOVS_MENU = 'jsons/paovs_menu.json'
MATH_MENU = 'jsons/math_menu.json'

positive_answer_array = []

negative_answer_array = []

phys_files = {}

termeh_files = {}

math_files = {}

paovs_files = {}


def collect_answer_array():
    f = open('config_files/go_or_not_answers.txt')
    writing_positive_answers = 1
    for line in f:
        if line.replace('\n', '') == 'positive':
            continue
        if line.replace('\n', '') == 'negative':
            writing_positive_answers = 0
        if writing_positive_answers:
            positive_answer_array.append(line.replace('\n', ''))
        if not writing_positive_answers:
            negative_answer_array.append(line.replace('\n', ''))


def collect_dictionaries():
    f = open('config_files/math_files.txt')
    fill_dic(math_files, f)
    f.close()
    f = open('config_files/phys_files.txt')
    fill_dic(phys_files, f)
    f.close()
    f = open('config_files/termeh_files.txt')
    fill_dic(termeh_files, f)
    f.close()
    f = open('config_files/paovs_files.txt')
    fill_dic(paovs_files, f)
    f.close()


def fill_dic(dic, filestream):
    for line in filestream:
        dic[line.split(':')[0]] = line.split(':')[1].replace('\n', '')


class Bot:
    def __init__(self):
        collect_dictionaries()
        collect_answer_array()
        # activate vk_api
        token = ''
        f = open('config_files/config_file.txt')
        for line in f:
            if line.split(':')[0] == 'token':
                token = line.split(':')[1].replace('\n', '')

        self.vk = vk_api.VkApi(token=token)
        self.vk._auth_token()

        # activate uploader
        self.vk_upld = vk_api.VkUpload(self.vk)

        # activate database
        self.database = Database()

    def start(self):
        print('Бот начал работу')
        while True:
            messages = self.vk.method('messages.getConversations',
                                      {'offset': 0,
                                       'count': 20,
                                       'filter': 'unanswered'})

            if messages['count'] > 0:
                text = messages['items'][0]['last_message']['text']
                user_id = messages['items'][0]['last_message']['from_id']
                user = self.vk.method("users.get", {"user_ids": user_id})
                name = user[0]['first_name']
                surname = user[0]['last_name']
                # attachments = messages['items'][0]['last_message']['attachments']

                self.database.insert_record_in_users(user_id, name, surname, 'user')

                """if attachments:
                    attachments = messages['items'][0]['last_message']['attachments'][0].get('type')"""

                user_state = self.database.get_user_state(user_id)

                # Init menu 
                if user_state == INIT_STATE:
                    if text.lower() == 'привет':
                        self.__simple_message_send(user_id, 'Привет. Я выдаю домашку, вот, держи клавиатуру, выбирай)'
                                                   , INIT_KEYBOARD_MENU)

                    elif text.lower() == 'идти сегодня на учебу?':
                        self.__in_university_go_ask(user_id)

                    elif text.lower() == 'скинь условия дз':
                        self.__simple_message_send(user_id, 'Выбирай предмет :0', HW_CHOOSING_SUBJECT_MENU)
                        self.database.change_user_state(user_id, HW_CHOOSING_STATE)
                    else:
                        self.__simple_message_send(user_id, 'Я всего лишь выдаю домашку...'
                                                            'Для тех, кто без клавы стуктура диалога\n'
                                                            '\nвы здесь < ("идти сегодня на учебу?" "скинь условия дз")'
                                                            ' < выбор предмета (теор. мех, '
                                                            'физика лабы, физика дз, паовс, назад)\n'
                                                            ' < выбор файла '
                                                            '(дз1/дз2/дз3/дз4, e-1/э-3...(которые у нас по курсу), '
                                                            '*дз-задачи-1/2/3*дз-задача-4(* - разделитель), '
                                                            'дз1/дз2, назад)',
                                                   INIT_KEYBOARD_MENU)

                # homework choosing menu
                elif user_state == HW_CHOOSING_STATE:
                    if text.lower() == 'теор. мех.':
                        self.__simple_message_send(user_id, 'Выбирай дзшку', TERMEH_MENU)
                        self.database.change_user_state(user_id, TERMEH_STATE)

                    elif text.lower() == 'физика лабы':
                        self.__simple_message_send(user_id, 'выбирай лабы', PHYS_LAB_MENU)
                        self.database.change_user_state(user_id, PHYS_LAB_STATE)
                    elif text.lower() == 'физика дз':
                        self.__simple_message_send(user_id, 'выбирай дз', PHYS_HW_MENU)
                        self.database.change_user_state(user_id, PHYS_HW_STATE)

                    elif text.lower() == 'паовс':
                        self.__simple_message_send(user_id, 'Копейкин лучший!', PAOVS_MENU)
                        self.database.change_user_state(user_id, PAOVS_STATE)
                    elif text.lower() == 'панкраты':
                        self.__simple_message_send(user_id,
                                                   'Ну давай, что ты еще будешь делать? Куда столько развития в один '
                                                   'день????',
                                                   MATH_MENU)
                        self.database.change_user_state(user_id, MATH_STATE)

                    elif text.lower() == 'назад':
                        self.__go_back_to_init_menu(user_id)
                    else:
                        self.__simple_message_send(user_id, 'Ничего не понимаю',
                                                   HW_CHOOSING_SUBJECT_MENU)

                elif user_state == TERMEH_STATE:
                    self.__document_choosing_aggregator(user_id, termeh_files, 'termeh', text,
                                                        'Ничего не понимаю',
                                                        TERMEH_MENU)
                elif user_state == PHYS_LAB_STATE:
                    self.__document_choosing_aggregator(user_id, phys_files, 'phys', text,
                                                        'Ничего не понимаю',
                                                        PHYS_LAB_MENU)
                elif user_state == PHYS_HW_STATE:
                    self.__document_choosing_aggregator(user_id, phys_files, 'phys', text,
                                                        'Ничего не понимаю',
                                                        PHYS_HW_MENU)
                elif user_state == PAOVS_STATE:
                    self.__document_choosing_aggregator(user_id, paovs_files, 'paovs', text,
                                                        'Ничего не понимаю',
                                                        PAOVS_MENU)
                elif user_state == MATH_STATE:
                    self.__document_choosing_aggregator(user_id, math_files, 'math', text,
                                                        'Ничего не понимаю',
                                                        MATH_MENU)

    def __in_university_go_ask(self, user_id):
        if random.randint(0, 100) < 70:
            self.vk.method('messages.send',
                           {'user_id': user_id,
                            'message': positive_answer_array[random.randint(0, len(positive_answer_array)) - 1],
                            'keyboard': open(INIT_KEYBOARD_MENU, "r", encoding="UTF-8").read(),
                            'random_id': random.randint(0, 1000)})
        else:
            self.vk.method('messages.send',
                           {'user_id': user_id,
                            'message': negative_answer_array[random.randint(0, len(negative_answer_array)) - 1],
                            'keyboard': open(INIT_KEYBOARD_MENU, "r", encoding="UTF-8").read(),
                            'random_id': random.randint(0, 1000)})

    def __get_document_sending_attach(self, user_id, filepath, result_name):
        upload_url = self.vk.method('docs.getMessagesUploadServer',
                                    {"type": "doc",
                                     "peer_id": user_id})['upload_url']
        response = requests.post(upload_url, files={'file': open(filepath, 'rb')})
        result = json.loads(response.text)
        file = result['file']

        json1 = self.vk.method('docs.save',
                               {'file': file,
                                'title': result_name,
                                'tags': []})

        owner_id = json1['doc']['owner_id']
        media_id = json1['doc']['id']

        attach = 'doc' + str(owner_id) + '_' + str(media_id)
        return attach

    def __document_message_send(self, user_id, hw_name, subject, result_name, keyboard):
        filepath = self.database.get_filepath(hw_name, subject)
        attach = self.__get_document_sending_attach(user_id, filepath, result_name)
        self.vk.method('messages.send',
                       {'user_id': user_id,
                        'attachment': attach,
                        'keyboard': open(keyboard, "r", encoding="UTF-8").read(),
                        'random_id': random.randint(0, 1000)})

    def __simple_message_without_keyboard_send(self, user_id, message):
        self.vk.method('messages.send',
                       {'user_id': user_id,
                        'message': message,
                        'random_id': random.randint(0, 1000)})

    def __simple_message_send(self, user_id, message, keyboard):
        self.vk.method('messages.send',
                       {'user_id': user_id,
                        'message': message,
                        'keyboard': open(keyboard, "r", encoding="UTF-8").read(),
                        'random_id': random.randint(0, 1000)})

    def __go_back_to_init_menu(self, user_id):
        self.vk.method('messages.send',
                       {'user_id': user_id,
                        'message': 'Назад так назад',
                        'keyboard': open(INIT_KEYBOARD_MENU, "r", encoding="UTF-8").read(),
                        'random_id': random.randint(0, 1000)})
        self.database.change_user_state(user_id, 0)

    def __go_back_to_subj_choose_menu(self, user_id):
        self.vk.method('messages.send',
                       {'user_id': user_id,
                        'message': 'Назад так назад',
                        'keyboard': open(HW_CHOOSING_SUBJECT_MENU, "r", encoding="UTF-8").read(),
                        'random_id': random.randint(0, 1000)})
        self.database.change_user_state(user_id, 1)

    def __document_choosing_aggregator(self, user_id, dictionary, subject, msg_from_text, stupid_msg, current_keyboard):
        if msg_from_text.lower() in dictionary:
            self.__document_message_send(user_id, dictionary[msg_from_text.lower()], subject,
                                         subject + '_' + dictionary[msg_from_text.lower()], current_keyboard)
        elif msg_from_text.lower() == 'назад':
            self.__go_back_to_subj_choose_menu(user_id)
        else:
            self.__simple_message_send(user_id, stupid_msg, current_keyboard)

create table users(
    user_id integer NOT NULL UNIQUE,
    name varchar(255) NOT NULL ,
    surname varchar(255) NOT NULL ,
    rules varchar(255) NOT NULL
);

create table user_states(
    user_id integer NOT NULL UNIQUE,
    user_state integer NOT NULL
);

create table task_files(
    name varchar(255) NOT NULL,
    subject varchar(255) NOT NULL,
    file varchar(255) NOT NULL UNIQUE
);

